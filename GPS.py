import ubxserial as ubx
import threading #Threading
import time , datetime
import os
import decoder as dec
import argparse , sys
import socket
import traceback
import logging


exit = 0
args = None
timeflag = 0

GlobalData = None
LastHeartBeat = 0;
device = None
remote = None

class BlinkerThread(threading.Thread):
  def __init__(self, threadID):
    threading.Thread.__init__(self)
    self.threadID = threadID

  def run(self):
    print("Starting BlinkerThread")
    Blinker()
    print("Exiting BlinkerThread")


class ReaderThread(threading.Thread):
  def __init__(self, threadID):
    threading.Thread.__init__(self)
    self.threadID = threadID


  def run(self):
    print("Starting ReaderThread")
    StartReader()
    print("Exiting ReaderThread")

class ServerThread(threading.Thread):
  def __init__(self, threadID):
    threading.Thread.__init__(self)
    self.threadID = threadID

  def run(self):
    print("Starting ServerThread")
    StartServer()
    print("Exiting ServerThread")


def Blinker():
  while not exit:
    print("Blinking .... ")
    time.sleep(2.0)


def StartReader():
    quit = 0
    global exit
    global LastHeartBeat
    global device

    global GlobalData
    count = 0;
    epc = datetime.datetime.now().timestamp()
    Ser = None
    UBX  = ubx.UBXReader(args.port , args.baud , 0.1 , args.serialDebug)
    ard = ubx.UBXReader("/dev/serial0" , 38400 , 0.1 , "Debug" )
    Decoder = dec.UBXDecoder(args.decoderDebug)
    GlobalData = Decoder


    #Logger Setting Here
    #formatter = logging.Formatter('%(asctime)s:%(name)s:%(levelname)s:%(message)s')
    formatter = logging.Formatter('%(asctime)s | %(message)s')
    handler2 = logging.FileHandler( os.path.dirname(os.path.realpath(__file__)) +  '/Logs/SonobotData.txt')
    handler2.setFormatter(formatter)
    Logger = logging.getLogger('Sonobot')
    Logger.setLevel(logging.INFO)
    Logger.addHandler(handler2)

    while quit is 0:
        count = count + 1
        time.sleep(0.001)
        UBX.OpenSerial()
        ard.OpenSerial()
        data = UBX.ReadLine()
        Decoder.DecodeString(data)
        if exit is 1:
          quit = 1;
          UBX.CloseSerial()
        if(count > 2):
          data = ard.ReadLine()
          if data:
            print(data)
            Logger.info(data)
            outputstr1 = '$GPS,' + str(GlobalData.ProcessedData['FixType']) + ',' + str(GlobalData.ProcessedData['Lon']) + ',' + str(GlobalData.ProcessedData['Vdop']) + ',' + str(GlobalData.ProcessedData['Hdop']) + ',' + str(GlobalData.ProcessedData['NSatellites']) + ',' + str(GlobalData.ProcessedData['Lat']) + ',' +  str(GlobalData.ProcessedData['Speed'])
            Logger.info(outputstr1)
          count = 0
        if (epc + 2 ) < datetime.datetime.now().timestamp():
          outputstr = '$GPS,' + str(GlobalData.ProcessedData['FixType']) + ',' + str(GlobalData.ProcessedData['Lon']) + ',' + str(GlobalData.ProcessedData['Vdop']) + ',' + str(GlobalData.ProcessedData['Hdop']) + ',' + str(GlobalData.ProcessedData['NSatellites']) + ',' + str(GlobalData.ProcessedData['Lat']) + ',' +  str(GlobalData.ProcessedData['Speed'])
          ard.Write(outputstr + '\r\n')
          #Send Data here
          epc = datetime.datetime.now().timestamp()
        if (LastHeartBeat) <  (datetime.datetime.now().timestamp() - 3):
          GlobalData.ProcessedData['Pdop'] = 0.0
        else:
          GlobalData.ProcessedData['Pdop'] = 1.0


def StartServer():
  host = "0.0.0.0"
  port = 8889         # arbitrary non-privileged port

  soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  soc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
  print("Socket created")

  try:
    soc.bind((host, port))
  except:
    print("Bind failed. Error : " + str(sys.exc_info()))
    sys.exit()

  soc.listen(5)       # queue up to 5 requests
  print("Socket now listening")
  soc.settimeout(5.0)


  while not exit:
    try:
      connection, address = soc.accept()
    except Exception as e:
      continue

    ip, port = str(address[0]), str(address[1])
    print("Connected with " + ip + ":" + port)

    try:
      threading.Thread(target=ClientThread, args=(connection, ip, port)).start()
    except:
      print("Thread did not start.")
      traceback.print_exc()

  soc.close()


def ClientThread(connection, ip, port):
    is_active = True
    #Send Data Schema Here
#    try:
#      output = ','.join("%r" % (key) for (key,val) in GlobalData.ProcessedData.iteritems())
#      connection.send('$EVOUBX,' + output.replace('\'' , '') + '\r\n')
#    except Exception as e:
#      print('Exception in sending TCP Data ' , str(e))
    while is_active and not exit:
      try:
        #output = ','.join("%r" % (val) for (key,val) in GlobalData.ProcessedData.items())
        output = '$EVOUBX,' + str(GlobalData.ProcessedData['FixType']) + ',0,' + str(GlobalData.ProcessedData['Lon']) + ','+ str(GlobalData.ProcessedData['Pdop']) + ',0,0,' + str(GlobalData.ProcessedData['Vdop']) + ',' + str(GlobalData.ProcessedData['Hdop']) + ',' + str(GlobalData.ProcessedData['NSatellites']) + ',' + str(GlobalData.ProcessedData['Lat']) + ',' + str(GlobalData.ProcessedData['Freq']) + ',' +  str(GlobalData.ProcessedData['Speed'])
        #sendstr = '$EVOUBX,' + output.replace('\'' , '') + '\r\n'
        output = output + '\r\n'
        connection.send(output.encode())
      except Exception as e:
        print('Exception in sending TCP Data ' , str(e))
        is_active = False

      time.sleep(1.0)


def main():
  global args
  global remote
  global device
  parser=argparse.ArgumentParser()
  parser.add_argument('--port', help='Port for UBX Receiver')
  parser.add_argument('--baud', help='BaudRate for UBX Receiver')
  parser.add_argument('--decoderDebug', help='NMEA Strig Decoder Log Level' , default = 'INFO')
  parser.add_argument('--serialDebug', help='Serial port Log Level' , default = 'NONE')
  args = parser.parse_args()

  if args.port is None or args.baud is None :
    print('Usage : python Gps.py --port <Port Name> --Baud <Baud Rate> ')
    sys.exit(1)

#  print('Bring up CAN0....')
#  os.system("sudo /sbin/ip link set can0 up type can bitrate 500000")

  # blinker = BlinkerThread(1)
  # blinker.start()

  reader = ReaderThread(2)
  reader.start()
  server = ServerThread(3)
  server.start()
  time.sleep(1)
#  try:
#    bus = can.interface.Bus(channel='can0', bustype='socketcan_native')
#  except OSError:
#    print('Cannot find PiCAN board.')
#    exit()

  while not exit:
#    message = bus.recv()	# Wait until a message is received.
#    c = '{0:f} {1:x} {2:x} '.format(message.timestamp, message.arbitration_id, message.dlc)
#    s=''
#    for i in range(message.dlc ):
#      s +=  '{0:x} '.format(message.data[i])
#      print(' {}'.format(c+s))
    time.sleep(1)


if __name__ == "__main__":
  try:
    main()
  except KeyboardInterrupt:
#    os.system("sudo /sbin/ip link set can0 down")
    exit = 1
    print("DONE... Program Exiting...\n\n")
