import pynmea2
import os
import logging
import time
import enum
import datetime
import json
import decimal

#{'GLL': <GLL(lat='5232.31385', lat_dir='N', lon='01323.10215', lon_dir='E', timestamp=datetime.time(8, 34, 45), status='A', faa_mode='A')>, 
# 'VTG': <VTG(true_track=None, true_track_sym='T', mag_track=None, mag_track_sym='M', spd_over_grnd_kts=Decimal('0.373'), spd_over_grnd_kts_sym='N', spd_over_grnd_kmph=0.691, spd_over_grnd_kmph_sym='K', faa_mode='A')>, 
# 'GSV': <GSV(num_messages='3', msg_num='3', num_sv_in_view='10', sv_prn_num_1='85', elevation_deg_1='73', azimuth_1='342', snr_1='18', sv_prn_num_2='86', elevation_deg_2='26', azimuth_2='275', snr_2='20')>,
# 'GSA': <GSA(mode='A', mode_fix_type='3', sv_id01='75', sv_id02='85', sv_id03='76', sv_id04='', sv_id05='', sv_id06='', sv_id07='', sv_id08='', sv_id09='', sv_id10='', sv_id11='', sv_id12='', pdop='4.32', hdop='1.54', vdop='4.03')>,
# 'RMC': <RMC(timestamp=datetime.timestamp(8, 34, 46), status='A', lat='5232.31377', lat_dir='N', lon='01323.10209', lon_dir='E', spd_over_grnd=0.373, true_course=None, datestamp=datetime.date(2018, 9, 12), mag_variation='', mag_var_dir='') data=['A']>, 

class SentenceID(enum.Enum):
	GGA = 0
	RMC = 1
	GSV = 2
	VTG = 3
	GSA = 4
	GLL = 5

class FixQuality(enum.Enum):
	INVALID = 0
	GPSFIX = 1
	DGPSFIX = 2
	PPSFIX = 3
	RTKFIX = 4
	FLRTKFIX = 5



class UBXDecoder:
	def __init__(self , LogLevel):
		self.LogFileName = os.path.dirname(os.path.realpath(__file__)) + '/Logs/Logs.txt'
		self.RawFileName = os.path.dirname(os.path.realpath(__file__)) + '/Logs/Raw.txt'
		self.ProcessedFileName = os.path.dirname(os.path.realpath(__file__)) + '/Logs/ProcessData.txt'
		self.LogLevel = None
		self.__DecodedData  = {'GGA':None , 'RMC':None , 'GLL':None , 'GSV':None , 'VTG':None , 'GSA':None , 'GST' : None}
		self.__LastRead = 0
		self.ProcessedData = {'FixType':0 , 'NSatellites' : 0 , 'Altitude' : 0.0 , 'Pdop' : 0.0 , 'Hdop' : 0.0 , 'Vdop' : 0.0 , 'Speed' : 0.0 , 'Lat' : 0.0 , 'Lon' : 0.0 , 'FixStatus' : None , 'Time': 0  , 'Freq' : 0.0}
		if LogLevel == 'DEBUG':
			self.LogLevel = logging.DEBUG
		elif LogLevel == 'INFO':
			self.LogLevel = logging.INFO
		elif LogLevel == 'WARNING':
			self.LogLevel = logging.WARNING
		else:
			self.LogLevel = 'NONE'

		formatter = logging.Formatter('%(asctime)s:%(name)s:%(levelname)s:%(message)s')
		formatter1 = logging.Formatter('%(asctime)s | %(message)s')

		handler = logging.FileHandler(self.LogFileName)
		handler.setFormatter(formatter)
		self.logger = logging.getLogger('GPSDecoder')
		self.logger.setLevel(self.LogLevel)
		self.logger.addHandler(handler)
		self.TimeSet = 0



		handler1 = logging.FileHandler(self.RawFileName)
		handler1.setFormatter(formatter1)
		self.raw = logging.getLogger('RawLogger')
		self.raw.setLevel(logging.INFO)
		self.raw.addHandler(handler1)

		handler2 = logging.FileHandler(self.ProcessedFileName)
		handler2.setFormatter(formatter1)
		self.processedLogger = logging.getLogger('ProcessedLogger')
		self.processedLogger.setLevel(logging.INFO)
		self.processedLogger.addHandler(handler2)


	def ProcessData(self , Type):
		try:
			if self.__LastRead is 0 and Type is SentenceID.RMC.value:
				self.__LastRead = int(round(time.time() * 1000))
			elif self.__LastRead is not 0 and Type is 1:
				timeNow = int(round(time.time() * 1000))
				self.ProcessedData['Freq'] = round(((1000.0 /  (timeNow - self.__LastRead))), 2 )
				self.__LastRead = timeNow

			if Type is SentenceID.GGA.value:
				self.ProcessedData['FixType'] =  self.__DecodedData['GGA'].gps_qual
				self.ProcessedData['NSatellites'] =  int(self.__DecodedData['GGA'].num_sats)
				self.ProcessedData['Altitude'] = self.__DecodedData['GGA'].altitude
#			elif Type is SentenceID.GSA.value:
				#self.ProcessedData['Pdop'] =  float(self.__DecodedData['GSA'].pdop)
				#self.ProcessedData['Hdop'] =  float(self.__DecodedData['GSA'].hdop)
#				#self.ProcessedData['Vdop'] = float(self.__DecodedData['GSA'].vdop)
			elif Type is SentenceID.VTG.value:
				self.ProcessedData['Speed'] =  self.__DecodedData['VTG'].spd_over_grnd_kmph
			elif Type is SentenceID.RMC.value:
				LatDec  = round(float(self.__DecodedData['RMC'].lat[:2]) + ((float(self.__DecodedData['RMC'].lat[2:])) / 60) , 9)
				LonDec = round(float(self.__DecodedData['RMC'].lon[:3]) + ((float(self.__DecodedData['RMC'].lon[3:])) / 60) , 9)

				if self.__DecodedData['RMC'].lat_dir is 'N':
					self.ProcessedData['Lat'] = LatDec
				elif self.__DecodedData['RMC'].lat_dir is 'S':
					self.ProcessedData['Lat'] = -LatDec

				if self.__DecodedData['RMC'].lon_dir is 'E':
					self.ProcessedData['Lon'] = LonDec
				elif self.__DecodedData['RMC'].lon_dir is 'W':
					self.ProcessedData['Lon'] = -LonDec
#				if self.__DecodedData['RMC'].status is 'A' and self.TimeSet is 0:
#					print ('Setting Time here !!')
				self.ProcessedData['FixStatus'] = self.__DecodedData['RMC'].status
				self.ProcessedData['Time'] = time.mktime(datetime.datetime.combine(self.__DecodedData['RMC'].datestamp, self.__DecodedData['RMC'].timestamp).timetuple())
				#Save as Json in another file.
				js = json.dumps(self.ProcessedData)
				self.processedLogger.info(str(js))
			elif Type is SentenceID.GST.value:
				print ('GST Value !!!!')
		except Exception as e:
			self.logger.error( 'Exception in Process Data ' + str(e))



	def DecodeString(self , data):
		if data:
			self.raw.info(data)
			try:
				newstr = str(data)
				if 'GST' in newstr:
					#$GPGST,172814.0,0.006,0.023,0.020,273.6,0.023,0.020,0.031*6A
					str1 = newstr.split('*')
					splitstr = str1[0].split(',')
					#self.ProcessedData['Pdop'] =  float(splitstr[8])
					self.ProcessedData['Hdop'] =  float(splitstr[6])
					self.ProcessedData['Vdop'] = float(splitstr[7])
				else :
					#self.logger.error('In OtheLoop')
					dec = pynmea2.parse(data.decode(), check=True)
					if isinstance(dec, pynmea2.types.talker.GGA):
						self.logger.info('GGA String')
						self.__DecodedData['GGA'] = dec
						self.ProcessData(SentenceID.GGA.value)
					elif isinstance(dec, pynmea2.types.talker.RMC):
						self.logger.info('RMC String')
						self.__DecodedData['RMC'] = dec
						self.ProcessData(SentenceID.RMC.value)
					elif isinstance(dec, pynmea2.types.talker.GSV):
						self.logger.info('GSV String')
						self.__DecodedData['GSV'] = dec
						self.ProcessData(SentenceID.GSV.value)
					elif isinstance(dec, pynmea2.types.talker.VTG):
						self.logger.info('VTG String')
						self.__DecodedData['VTG'] = dec
						self.ProcessData(SentenceID.VTG.value)
					elif isinstance(dec, pynmea2.types.talker.GSA):
						self.logger.info('GSA String')
						self.__DecodedData['GSA'] = dec
						self.ProcessData(SentenceID.GSA.value)
					elif isinstance(dec, pynmea2.types.talker.GLL):
						self.logger.info('GLL String')
						self.__DecodedData['GLL'] = dec
						self.ProcessData(SentenceID.GLL.value)
					elif isinstance(dec, pynmea2.types.talker.TXT):
						self.logger.info('TXT String')
					elif isinstance(dec, pynmea2.types.talker.ZDA):
						self.logger.info('ZDA String')
					else:
						self.logger.info('Unknown string....')
						return

			except Exception as e:
				self.logger.error('Exception while parsing GPS string %s ' % str(e))
