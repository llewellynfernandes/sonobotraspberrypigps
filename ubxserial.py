import time
import serial
import os
import logging


class UBXReader:
  def __init__(self, Port, Baud, SerialTimeout,  LogLevel):
    self.LogFileName = os.path.dirname(os.path.realpath(__file__)) + '/Logs/Logs.txt'
    self.Port = Port
    self.Baud = Baud
    self.Ser = None
    self.SerialTimeout = SerialTimeout

    self.LogLevel = None
    if LogLevel == 'DEBUG':
      self.LogLevel = logging.DEBUG
    elif LogLevel == 'INFO':
      self.LogLevel = logging.INFO
    elif LogLevel == 'WARNING':
      self.LogLevel = logging.WARNING
    else:
      self.LogLevel = logging.NOTSET
    formatter = logging.Formatter('%(asctime)s:%(name)s:%(levelname)s:%(message)s')

    handler = logging.FileHandler(self.LogFileName)
    handler.setFormatter(formatter)

    self.logger = logging.getLogger('UBXSerial')
    self.logger.setLevel(self.LogLevel)
    self.logger.addHandler(handler)



  def OpenSerial(self):
    try:
      if self.Ser is None:
        self.Ser = serial.Serial(self.Port, self.Baud, timeout = self.SerialTimeout)
        self.logger.info('port opened \r\n')
    except serial.SerialException as e:
      self.logger.error('could not connect to %s \r\n Exception %s ' % (self.Port , str(e)))
      time.sleep(5.0)
      self.Ser = None


  def CloseSerial(self):
    try:
      if self.Ser is not None:
        self.Ser.close()
        self.logger.info('SerialPort closed ...')
    except serial.SerialException as e:
      logger.error('could not close port\r\n Exception %s ' % str(e))


  def ReadLine(self):
    try:
      if self.Ser is not None:
        read  = self.Ser.readline()
        self.logger.info(read)
        return read
      else:
        return None
    except Exception as e:
      self.logger.error('Read Exception %s ' % str(e))

  def Write(self , Data):
    try:
      if self.Ser is not None:
        self.Ser.write(Data.encode())
    except Exception as e:
      print("Serial Write Exception" , str(e))
      self.logger.error('Write Exception %s ' % str(e))

